#include <jsc/memory/pool.h>

namespace jsc {

jsc_noinline
MemoryPool::~MemoryPool() {
	auto block = this->block;
	while (block) {
		auto prev_block = block->prev;
		memory_free((u8*)block, block->size);
		block = prev_block;
	}
}

jsc_noinline
u8* MemoryPool::allocate_slow(uword size) {
	if (size >= policy.large_size) {
		return allocate_block(size + sizeof(AllocationBlock));
	} else {
		u8* ptr = allocate_block(policy.block_size);
		cur = ptr + size;
		end = ptr + policy.block_size - sizeof(AllocationBlock);
		return ptr;
	}
}

u8* MemoryPool::allocate_block(uword total_size) {
	u8* ptr = memory_allocate(total_size);
	AllocationBlock* block = (AllocationBlock*)ptr;
	block->prev = this->block;
	block->size = total_size;
	this->block = block;
	return ptr + sizeof(AllocationBlock);
}

}
