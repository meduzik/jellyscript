#include <jsc/memory/memory.h>
#include <jsc/memory/arena.h>
#include <jsc/macro/panic.h>

namespace jsc {

static constexpr uword ArenaChunkHeaderSize = sizeof(u8*);
static constexpr uword ArenaChunkSize = 1024 * 1024 * 2;
static constexpr uword ArenaChunkPayload = ArenaChunkSize - ArenaChunkHeaderSize;

static constexpr uword ArenaLargeAllocationThreshold = 256 * 1024;

static u8* ArenaChunkAlloc() {
	u8* mem = (u8*)memory_allocate(ArenaChunkSize);
	if (!mem) {
		jsc_panic("memory allocation failed");
	}
	return mem;
}

static void ArenaChunkFree(u8* ptr) {
	memory_free(ptr, ArenaChunkSize);
}

static ArenaLargeChunk* ArenaLargeChunkAllocate(uword size) {
	ArenaLargeChunk* chunk = (ArenaLargeChunk*)memory_allocate(sizeof(ArenaLargeChunk) + size);
	chunk->size = size;
	if (!chunk) {
		jsc_panic("memory allocation failed");
	}
	return chunk;
}

static void ArenaLargeChunkFree(ArenaLargeChunk* chunk) {
	memory_free((u8*)chunk, chunk->size + sizeof(ArenaLargeChunk));
}


ArenaPool::ArenaPool(ArenaPoolState state) :
	state(state),
	cache_chunk(nullptr),
	level(0),
	maxlevel(0)
{
}

static thread_local ArenaPool ArenaThreadLocalPool({nullptr, nullptr});

ArenaPool* ArenaPool::GetThreadLocal() {
	return &ArenaThreadLocalPool;
}

Arena::Arena(): 
	Arena(ArenaPool::GetThreadLocal())
{
}

Arena::Arena(ArenaPool* pool):
	pool(pool),
	reset(pool->state),
	reset_level(pool->level),
	level(pool->maxlevel + 1),
	large_chunk(nullptr)
{
	pool->maxlevel++;
	pool->level = level;
}

jsc_noinline
Arena::~Arena() {
	auto pool = this->pool;
	if (jsc_unlikely(pool->level == level || large_chunk)) {
		auto pool = this->pool;
		if (pool->level == level) {
			pool->level = reset_level;
			auto pool_end = pool->state.end;
			auto reset_end = reset.end;
			while (pool_end != reset_end) {
				u8* pool_begin = pool_end - ArenaChunkPayload;
				u8* next_pool = *(u8**)pool_end;
				if (pool->cache_chunk) {
					ArenaChunkFree(pool->cache_chunk);
				}
				pool->cache_chunk = pool_begin;
				pool_end = next_pool;
			}
			pool->state = reset;
		}
		auto large_chunk = this->large_chunk;
		while (large_chunk) {
			auto next_chunk = large_chunk->next;
			ArenaLargeChunkFree(large_chunk);
			large_chunk = next_chunk;
		}
	}
	pool->maxlevel--;
}

u8* Arena::allocate_slow_path(uword size) {
	if (size >= ArenaLargeAllocationThreshold) {
		return allocate_large(size);
	}

	auto pool = this->pool;
	u8* next_chunk = pool->cache_chunk;
	if (next_chunk) {
		pool->cache_chunk = nullptr;
	} else {
		next_chunk = ArenaChunkAlloc();
	}

	auto cur = next_chunk;
	auto end = cur + ArenaChunkPayload;
	*(u8**)end = pool->state.end;
	pool->level = std::min(pool->level, level);
	pool->state = { cur + size, end };
	return cur;
}

u8* Arena::allocate_large(uword size) {
	ArenaLargeChunk* large_chunk = ArenaLargeChunkAllocate(size);
	large_chunk->next = this->large_chunk;
	this->large_chunk = large_chunk;
	return ((u8*)large_chunk) + sizeof(ArenaLargeChunk);
}

}
