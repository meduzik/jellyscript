#include <jsc/common.h>

void jsc_default_panic_handler(void*, const char* file, int line, const char* function, const char* message){
	fprintf(stderr, "PANIC %s %s(%d): %s", file, function, line, message);
	abort();
}

static jsc_panic_handler jsc_panic_handler_instance{nullptr, jsc_default_panic_handler};

JSC_C jsc_panic_handler jsc_panic_handler_set(jsc_panic_handler new_handler){
	jsc_panic_handler old_handler = jsc_panic_handler_instance;
	jsc_panic_handler_instance = new_handler;
	return old_handler;
}

JSC_C jsc_panic_handler jsc_panic_handler_get(){
	return jsc_panic_handler_instance;
}

JSC_C jsc_noreturn
void jsc_raise_panic(const char* file, int line, const char* function, const char* message){
	jsc_panic_handler_instance.func(jsc_panic_handler_instance.ud, file, line, function, message);
	abort();
}
