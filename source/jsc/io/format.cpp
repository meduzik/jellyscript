#include <jsc/io/format.h>

namespace jsc {

namespace format {

static uword int_to_buffer(u64 val, u8* buf) {
	u8 tmp[32];
	u8* out = tmp + sizeof(tmp);
	do {
		--out;
		*out = '0' + val % 10;
		val /= 10;
	} while (val);
	uword len = tmp + sizeof(tmp) - out;
	memcpy(buf, out, len);
	return len;
}

IntegerBuffer decimal(i64 val) {
	if (val < 0) {
		IntegerBuffer result;
#pragma warning(push)
#pragma warning(disable:4146)
		result.length = (u16)int_to_buffer(-(u64)val, result.buffer + 1);
#pragma warning(pop)
		result.length++;
		result.buffer[0] = '-';
		return result;
	} else {
		return decimal((u64)val);
	}
}

IntegerBuffer decimal(u64 val) {
	IntegerBuffer result;
	result.length = (u16)int_to_buffer(val, result.buffer);
	return result;
}

}



}
