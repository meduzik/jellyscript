#include <jsc/io/object_stream.h>

namespace jsc {

void ObjectStream::_value_null() {
}

void ObjectStream::_value_bool(bool val) {
}

void ObjectStream::_value_uint(u64 val) {
}

void ObjectStream::_value_int(i64 val) {
}

void ObjectStream::_value_float(float val) {
}

void ObjectStream::_value_double(double val) {
}

void ObjectStream::_value_string_begin() {
}

void ObjectStream::_value_string_end() {
}

void ObjectStream::_key_begin() {
}

void ObjectStream::_key_end() {
}

void ObjectStream::_object_begin() {
}

void ObjectStream::_object_end() {
}

void ObjectStream::_array_begin() {
}

void ObjectStream::_array_end() {
}

void ObjectStream::_feed_data(StringRef data) {
}


}
