#include "PrintFloat.h"
#include <jsc/io/format.h>

namespace jsc {

namespace format {

FloatBuffer scientific(float val) {
	FloatBuffer result;
	result.length = (u16)PrintFloat32((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Scientific, -1);
	return result;
}

DoubleBuffer scientific(double val) {
	DoubleBuffer result;
	result.length = (u16)PrintFloat64((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Scientific, -1);
	return result;
}

FloatBuffer scientific(float val, uword precision) {
	FloatBuffer result;
	result.length = (u16)PrintFloat32((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Scientific, (tS32)precision);
	return result;
}

DoubleBuffer scientific(double val, uword precision) {
	DoubleBuffer result;
	result.length = (u16)PrintFloat64((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Scientific, (tS32)precision);
	return result;
}

FloatBuffer fixed(float val) {
	FloatBuffer result;
	result.length = (u16)PrintFloat32((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Positional, -1);
	return result;
}

DoubleBuffer fixed(double val) {
	DoubleBuffer result;
	result.length = (u16)PrintFloat64((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Positional, -1);
	return result;
}

FloatBuffer fixed(float val, uword precision) {
	FloatBuffer result;
	result.length = (u16)PrintFloat32((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Positional, (tS32)precision);
	return result;
}

DoubleBuffer fixed(double val, uword precision) {
	DoubleBuffer result;
	result.length = (u16)PrintFloat64((tC8*)result.buffer, result.Size, val, PrintFloatFormat_Positional, (tS32)precision);
	return result;
}

FloatBuffer shortest(float val, uword allowed_fixed_len) {
	FloatBuffer r1;
	r1.length = (u16)PrintFloat32((tC8*)r1.buffer, r1.Size, val, PrintFloatFormat_Positional, -1);
	if (r1.length <= allowed_fixed_len) {
		return r1;
	}
	FloatBuffer r2;
	r2.length = (u16)PrintFloat32((tC8*)r2.buffer, r2.Size, val, PrintFloatFormat_Scientific, -1);
	if (r1.length < r2.length) {
		return r1;
	}
	return r2;
}

DoubleBuffer shortest(double val, uword allowed_fixed_len) {
	DoubleBuffer r1;
	r1.length = (u16)PrintFloat64((tC8*)r1.buffer, r1.Size, val, PrintFloatFormat_Positional, -1);
	if (r1.length <= allowed_fixed_len) {
		return r1;
	}
	DoubleBuffer r2;
	r2.length = (u16)PrintFloat64((tC8*)r2.buffer, r2.Size, val, PrintFloatFormat_Scientific, -1);
	if (r1.length < r2.length) {
		return r1;
	}
	return r2;
}


}

}
