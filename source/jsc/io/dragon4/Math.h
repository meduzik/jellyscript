/******************************************************************************
  Copyright (c) 2014 Ryan Juckett
  http://www.ryanjuckett.com/
 
  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.
 
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
 
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
 
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
 
  3. This notice may not be removed or altered from any source
     distribution.
******************************************************************************/

#ifndef RJ__Math_h
#define RJ__Math_h

#include "Standard.h"

#if _MSC_VER
	#include <intrin.h>

	inline tU32 Dragon4Log2Impl32(tU32 val) {
		unsigned long r;
		_BitScanReverse(&r, val);
		return r;
	}

	inline tU32 Dragon4Log2Impl64(tU64 val) {
		#if defined(_WIN64)
			unsigned long r;
			_BitScanReverse64(&r, val);
			return r;
		#else
			unsigned long hi;
			unsigned long lo;

			char high_set = _BitScanReverse(&hi, (tU32)(val >> 32));
			_BitScanReverse(&lo, (tU32)val);
			hi |= 32;

			return high_set ? hi : lo;
		#endif
	}

	#define DRAGON4_LOG2_32(val) (Dragon4Log2Impl32(val))
	#define DRAGON4_LOG2_64(val) (Dragon4Log2Impl64(val))
#else
	#define DRAGON4_LOG2_32(val) (31-__builtin_clz(val))
	#define DRAGON4_LOG2_64(val) ((tU32)(63-__builtin_clzll(val)))
#endif

#endif
