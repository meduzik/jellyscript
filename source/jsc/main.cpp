#include <stdio.h>
#include <jsc/parser/parser.h>
#include <jsc/io/diagnostic.h>
#include <jsc/ast/ast.h>
#include <fstream>
using namespace jsc;

Span<u8> read_file(MemoryPool* pool, StringRef filename) {
	std::ifstream file(std::string_view((const char*)filename.data(), filename.size()), std::ios::binary);
	if (!file) {
		abort();
	}
	file.seekg(0, std::ios_base::end);
	size_t file_size = file.tellg();
	u8* data = pool->allocate_checked(file_size);
	file.seekg(0, std::ios_base::beg);
	file.read((char*)data, file_size);
	return {data, file_size};
}

int main() {
	DiagnosticStream stream;
	
	for (int i = 0; i < 20; i++) {
		MemoryPool pool;
		StringRef filename("tests/huge.jsc");
		Parser* parser = parser_create();
		ParserJob job {
			&stream,
			filename,
			&pool,
			read_file(&pool, filename)
		};
		auto program = parser_run(parser, job);
		parser_destroy(parser);
	}
}
