#include <jsc/types/twine.h>

namespace jsc {

u8* Twine::write(u8* buf, const Twine* tw) {
	switch (tw->tag) {
	case twine_tag::empty: {
		return buf;
	} break;
	case twine_tag::string_ref: {
		return write(buf, tw->value.sr);
	} break;
	case twine_tag::concat_sr_sr: {
		buf = write(buf, tw->value.concat_srsr.lhs);
		buf = write(buf, tw->value.concat_srsr.rhs);
		return buf;
	} break;
	case twine_tag::concat_tw_sr: {
		buf = write(buf, tw->value.concat_tsr.lhs);
		buf = write(buf, tw->value.concat_tsr.rhs);
		return buf;
	} break;
	case twine_tag::concat_sr_tw: {
		buf = write(buf, tw->value.concat_srt.lhs);
		buf = write(buf, tw->value.concat_srt.rhs);
		return buf;
	} break;
	case twine_tag::concat_tw_tw: {
		buf = write(buf, tw->value.concat_tt.lhs);
		buf = write(buf, tw->value.concat_tt.rhs);
		return buf;
	} break;
	default: jsc_unreachable;
	}
}

u8* Twine::write(u8* buf, StringRef sr) {
	uword size = sr.size();
	memcpy(buf, sr.data(), size);
	return buf + size;
}

}

