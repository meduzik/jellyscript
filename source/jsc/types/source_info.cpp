#include <jsc/types/source_info.h>

namespace jsc {

SourceLoc SourceMap::get_loc(u32 pos) const {
	// lines[0] is always zero
	// lines[i] is where i-th line begins
	// lines[lines.size() - 1] is equal to the file size
	const u32* line = std::upper_bound(lines.begin(), lines.end(), pos);
	if (line == lines.end()) {
		// past the file size
		// return the last position in the file
		return get_end_loc();
	}
	--line;
	return {(u32)(line - lines.data()), pos - *line};
}

u32 SourceMap::get_pos(SourceLoc loc) const {
	uword size = lines.size();
	if (loc.line >= size - 1) {
		return lines[size - 1];
	}
	u32 line_length = lines[loc.line] - lines[loc.line + 1];
	if (loc.col >= line_length) {
		return lines[loc.line + 1];
	}
	return lines[loc.line] + loc.col;
}

SourceLoc SourceMap::get_end_loc() const {
	uword size = lines.size();
	return {(u32)(size - 2), lines[lines.size() - 1] - lines[lines.size() - 2]};
}

}