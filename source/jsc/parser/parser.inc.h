#pragma once
#include <jsc/ast/ast.h>
#include <jsc/memory/pool.h>
#include <jsc/types/vector.h>

namespace jsc::pars {

struct Callback {
	void* ud;
	MemoryPool* mem;
	
	void (*on_result) (void* ud, ast::File* file);
	void (*correction_remove) (void* ud, u32*& tokid);
	void (*correction_insert) (void* ud, u32*& tokid, u16 terminal);
	void (*correction_replace) (void* ud, u32*& tokid, u16 terminal);
	void (*sync_skip) (void* ud, u32*& tokid, uword num);
	void (*sync_insert) (void* ud, u32*& tokid, u16 terminal);
};

}
