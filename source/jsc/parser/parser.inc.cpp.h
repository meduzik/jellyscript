#pragma once
#include <jsc/types/span.h>
#include <jsc/memory/memory.h>

namespace jsc {

template<class T>
struct CList {
	CList<T>* prev;
	T val;

	CList(CList<T>* prev, T val) :
		prev(prev),
		val(val) {
	}

	static size_t size(CList<T>* node) {
		size_t n = 0;
		while (node) {
			n++;
			node = node->prev;
		}
		return n;
	}
};

template<class T>
static Span<T> flatten(CList<T>* node, MemoryPool* pool) {
	size_t size = CList<T>::size(node);
	T* arr = nullptr;
	if (size > 0) {
		arr = (T*)pool->allocate_checked(sizeof(T) * size);
		T* out = arr + size;
		while (node) {
			out--;
			*out = node->val;
			node = node->prev;
		}
	}
	return { arr, size };
}

}

#define mk(T, ...) cb.mem->create<T>(__VA_ARGS__)
#define mklist(head, tail) mk(CList<decltype(tail)>, head, tail)
#define flat(l) flatten(l, cb.mem)
