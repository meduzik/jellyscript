#include <jsc/parser/parser.h>
#include <jsc/memory/pool.h>
#include <jsc/types/vector.h>
#include <jsc/ast/ast.h>
#include "parser.gen.h"
#include "lexer.h"
#include "lexer.gen.h"
#include <iostream>

namespace jsc {

struct Parser {
};


enum class Token : u16 {
#define X(t,v,n) t = v,
	jsc_lex_TOKENS(X)
#undef X
};

const char* NameOf(u16 t) {
	switch (t) {
#define X(t,v,n) case v: return n;
		jsc_lex_TOKENS(X)
#undef X
	}
	return "<unknown_token>";
}

Parser* parser_create() {
	Parser* parser = Mem.create<Parser>();
	
	return parser;
}

static void compute_lines(Span<u8> data, pool_vector<u32>& lines) {
	const u8* data_begin = data.data();
	const u8* data_end = data.end();

	constexpr size_t ChunkSize = 64;

	u32 offset = 0;
	u32 data_size = (u32)(data_end - data_begin);
	lines.push_back(0);
	lines.push_back(0);

	size_t lines_count = 0;

	bool old_cr = false;
	while (offset < data_size) {
		lines.resize(lines_count + ChunkSize);
		u32* lines_pos = lines.data() + lines_count;
		u32* lines_end = lines.data() + lines.size() - 1;
		while (lines_pos < lines_end && offset < data_size) {
			u8 ch = data_begin[offset];
			bool cr = (ch == '\r');
			bool lf = (ch == '\n');
			offset++;
			lines_pos += (cr | (lf & !old_cr));
			if (cr | lf) {
				*lines_pos = offset;
			}
			old_cr = cr;
		}
		lines_count = lines_pos - lines.data();
	}
	lines.resize(lines_count + 1);
	lines.push_back(offset);
}

ParseResult parser_run(Parser* parser, const ParserJob& job) {
	MemoryPool temp;

	LexerResult lexer = lexer_run(job.mem, job.data);

	auto& tokens = lexer.tokens;
	auto& offsets = lexer.offsets;
	uword tokens_count = tokens.size();

	pool_vector<u16> input(tokens.size() + 1, &temp);
	pool_vector<u32> identities(tokens.size() + 2, &temp);

	const u16* tokens_in = tokens.data();
	u16* input_begin = input.data();
	u32* identities_begin = identities.data();
	u32 j = 0;
	identities_begin[0] = 0;
	identities_begin++;
	u32 i = 0;
	for (i = 0; i < tokens_count; i++) {
		u16 token = (u16)tokens_in[i];
		input_begin[j] = token;
		identities_begin[j] = i;
		j += !pars::skippable_flag[token];
	}
	input_begin[j] = (u16)Token::EoF;
	identities_begin[j] = i;
	tokens.push_back((u16)Token::EoF);
	offsets.push_back(offsets.back());

	pars::AllocatorCallback alc{
		&temp,
		[](void* ud, uword size) -> u8 * {
			MemoryPool* temp = (MemoryPool*)ud;
			return temp->allocate(size);
		},
		[](void* ud, u8* ptr, uword old_size, uword new_size) -> u8 * {
			MemoryPool* temp = (MemoryPool*)ud;
			return temp->reallocate(ptr, old_size, new_size);
		},
		[](void* ud, u8* ptr, uword size) -> void {
			MemoryPool* temp = (MemoryPool*)ud;
			return temp->free(ptr, size);
		}
	};


	pool_vector<u32> lines(job.mem);
	compute_lines(job.data, lines);

	SourceFile* file = job.mem->create<SourceFile>(
		job.filename,
		SourceMap(
			{ lines.data(), lines.size() }
		),
		SourceText{
			{tokens.data(), tokens.size()},
			{offsets.data(), offsets.size()},
			job.data
		}
	);

	pars::ParserConfig config = pars::DefaultConfig;
	config.chunk_size = 65000;
	config.data_initial = 16384;
	config.stack_initial = 65500;

	pars::ParserState* state = pars::parser_create(alc, config);

	struct CallbackData {
		pool_vector<u32>& offsets;
		pool_vector<u16>& tokens;
		ast::File* ast;
		SourceFile* file;
	} cb_data {
		offsets,
		tokens,
		nullptr,
		file
	};

	pars::Callback cb {
		&cb_data,
		job.mem,
		[](void* ud, ast::File* file) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			cb_data->ast = file;
		},
		[](void* ud, u32*& tokid) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			SourceLoc begin = cb_data->file->map.get_loc(cb_data->offsets[*tokid]);
			std::cout << "lec skips '" << NameOf(cb_data->tokens[*tokid]) << "' at line " << begin.line + 1 << ", col " << begin.col << "\n";
			tokid++;
		},
		[](void* ud, u32*& tokid, u16 terminal) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			u32 pos = cb_data->offsets[*tokid];
			SourceLoc begin = cb_data->file->map.get_loc(pos);
			std::cout << "lec inserts '" << NameOf(terminal) << "' at line " << begin.line + 1 << ", col " << begin.col << "\n";
			tokid--;
			*tokid = (u32)cb_data->offsets.size();
			cb_data->offsets.push_back(pos);
			cb_data->offsets.push_back(pos);
			cb_data->tokens.push_back(terminal);
			cb_data->tokens.push_back(0);
		},		
		[](void* ud, u32*& tokid, u16 terminal) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			u32 pos = cb_data->offsets[*tokid];
			SourceLoc begin = cb_data->file->map.get_loc(pos);
			std::cout << "lec replaces '" << NameOf(cb_data->tokens[*tokid]) 
				<< "' at line " << begin.line + 1 << ", col " << begin.col
				<< " with '" << NameOf(terminal) << "\n";
			*tokid = (u32)cb_data->offsets.size();
			cb_data->offsets.push_back(pos);
			cb_data->offsets.push_back(pos);
			cb_data->tokens.push_back(terminal);
			cb_data->tokens.push_back(0);
		},
		[] (void* ud, u32* &tokid, uword num) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			SourceLoc begin = cb_data->file->map.get_loc(cb_data->offsets[*tokid]);
			std::cout << "panic skips '" << NameOf(cb_data->tokens[*tokid]) 
				<< "' at line " << begin.line + 1 << ", col " << begin.col
				<< ")\n";
			tokid += num;
		},
		[] (void* ud, u32* &tokid, u16 terminal) -> void {
			CallbackData* cb_data = (CallbackData*)ud;
			u32 pos = cb_data->offsets[*tokid];
			SourceLoc begin = cb_data->file->map.get_loc(pos);
			std::cout << "panic inserts '" << NameOf(terminal) 
				<< "' at line " << begin.line + 1 << ", col " << begin.col
				<< "\n";
			tokid--;
			*tokid = (u32)cb_data->offsets.size();
			cb_data->offsets.push_back(pos);
			cb_data->offsets.push_back(pos);
			cb_data->tokens.push_back(terminal);
			cb_data->tokens.push_back(0);
		}
	};
	pars::ParseResult result = pars::parser_run(
		state,
		pars::NonTerminal::program,
		input_begin,
		input_begin + j,
		identities_begin,
		cb,
		&offsets
	);
	pars::parser_destroy(state);

	return {
		cb_data.ast,
		file,
	};
}

void parser_destroy(Parser* parser) {
	
}

}
