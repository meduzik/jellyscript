#include "lexer.h"
#include "lexer.gen.h"
#include <jsc/memory/allocator.h>
#include <jsc/memory/arena.h>
#include <jsc/types/vector.h>

namespace jsc {

struct LexerContext {
	MemoryPool* pool;
	pool_vector<uint16_t> tokens;
	pool_vector<uint32_t> offsets;
	size_t count;

	LexerContext(MemoryPool* pool, Arena* arena) :
		pool(pool),
		tokens(pool),
		offsets(pool),
		count(0)
	{
	}
};

LexerResult lexer_run(MemoryPool* mem, Span<u8> data) {
	Arena arena;
	LexerContext ctx(mem, &arena);

	ctx.offsets.push_back(0);

	lex::LexerCallback callback {
		&ctx,
		[] (
			void* ud,
			uint16_t* tokens,
			uint32_t* offsets,
			size_t count
		) {
			LexerContext* ctx = (LexerContext*)ud;
			ctx->count += count;
		},
		[] (
			void* ud,
			uint16_t** tokens,
			uint32_t** offsets,
			size_t* count
		) {
			LexerContext* ctx = (LexerContext*)ud;
			size_t size = 8 * 1024 - 64;
			ctx->tokens.resize(ctx->count + size);
			ctx->offsets.resize(ctx->count + 1 + size);
			*tokens = ctx->tokens.data() + ctx->count;
			*offsets = ctx->offsets.data() + ctx->count + 1;
			*count = size;
		}
	};
	lex::run(callback, data.data(), data.size());
	ctx.tokens.resize(ctx.count);
	ctx.offsets.resize(ctx.count + 1);

	LexerResult result {
		std::move(ctx.tokens),
		std::move(ctx.offsets)
	};
	return result;
}


}
