#pragma once

#include <cstddef>
#include <cstdint>

namespace jsc::lex {

#define jsc_lex_TOKENS(X) \
	X(K_import, 0, "import") \
	X(K_as, 1, "as") \
	X(K_class, 2, "class") \
	X(K_namespace, 3, "namespace") \
	X(K_interface, 4, "interface") \
	X(K_ref, 5, "ref") \
	X(K_struct, 6, "struct") \
	X(K_enum, 7, "enum") \
	X(K_function, 8, "function") \
	X(K_var, 9, "var") \
	X(K_const, 10, "const") \
	X(K_return, 11, "return") \
	X(K_if, 12, "if") \
	X(K_else, 13, "else") \
	X(K_while, 14, "while") \
	X(K_for, 15, "for") \
	X(K_break, 16, "break") \
	X(K_goto, 17, "goto") \
	X(K_continue, 18, "continue") \
	X(K_abstract, 19, "abstract") \
	X(K_final, 20, "final") \
	X(K_virtual, 21, "virtual") \
	X(K_override, 22, "override") \
	X(K_static, 23, "static") \
	X(K_public, 24, "public") \
	X(K_private, 25, "private") \
	X(K_internal, 26, "internal") \
	X(K_protected, 27, "protected") \
	X(Identifier, 28, "identifier") \
	X(Decimal, 29, "decimal_lit") \
	X(Float, 30, "float_lit") \
	X(Hex, 31, "hex_lit") \
	X(Bin, 32, "bin_lit") \
	X(Comment, 33, "comment") \
	X(MLComment, 34, "ml_comment") \
	X(String, 35, "string_lit") \
	X(OpAt, 36, "@") \
	X(LParen, 37, "(") \
	X(RParen, 38, ")") \
	X(LBracket, 39, "[") \
	X(RBracket, 40, "]") \
	X(LBrace, 41, "{") \
	X(RBrace, 42, "}") \
	X(OpHash, 43, "#") \
	X(OpAssign, 44, "=") \
	X(OpPlusAssign, 45, "+=") \
	X(OpMinusAssign, 46, "-=") \
	X(OpMulAssign, 47, "*=") \
	X(OpDivAssign, 48, "/=") \
	X(OpModAssign, 49, "%=") \
	X(OpAndAssign, 50, "&=") \
	X(OpOrAssign, 51, "|=") \
	X(OpXorAssign, 52, "^=") \
	X(OpShlAssign, 53, "<<=") \
	X(OpAShrAssign, 54, ">>=") \
	X(OpLShrAssign, 55, ">>>=") \
	X(OpIncr, 56, "++") \
	X(OpDecr, 57, "--") \
	X(OpLogicalInvert, 58, "!") \
	X(OpPlus, 59, "+") \
	X(OpMinus, 60, "-") \
	X(OpMul, 61, "*") \
	X(OpDiv, 62, "/") \
	X(OpMod, 63, "%") \
	X(OpBitInvert, 64, "~") \
	X(OpBitAnd, 65, "&") \
	X(OpBitOr, 66, "|") \
	X(OpLogicalOr, 67, "||") \
	X(OpLogicalAnd, 68, "&&") \
	X(OpBitXor, 69, "^") \
	X(OpShl, 70, "<<") \
	X(OpAShr, 71, ">>") \
	X(OpLShr, 72, ">>>") \
	X(OpEQ, 73, "==") \
	X(OpNEQ, 74, "!=") \
	X(OpLT, 75, "<") \
	X(OpGT, 76, ">") \
	X(OpLE, 77, "<=") \
	X(OpGE, 78, ">=") \
	X(OpArrow, 79, "->") \
	X(OpDot, 80, ".") \
	X(OpComma, 81, ",") \
	X(OpColon, 82, ":") \
	X(OpSemi, 83, ";") \
	X(OpQuestion, 84, "?") \
	X(OpColonColon, 85, "::") \
	X(EoF, 86, "eof") \
	X(Space, 87, "space") \
	X(Error, 88, "error") \



struct LexerCallback {
	void* ud;
	void (*on_output) (
		void* ud,
		uint16_t* tokens,
		uint32_t* offsets,
		size_t count
	);
	void (*get_buffer) (
		void* ud,
		uint16_t** tokens,
		uint32_t** offsets,
		size_t* count
	);
};

void run(LexerCallback cb, const uint8_t* data, size_t len);

}

