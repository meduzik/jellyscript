#pragma once

#include <jsc/types/span.h>
#include <jsc/memory/pool.h>
#include <jsc/types/vector.h>

namespace jsc {

enum class Token: u16;

struct LexerResult {
	pool_vector<u16> tokens;
	pool_vector<u32> offsets;
};

LexerResult lexer_run(MemoryPool* mem, Span<u8> data);

}
