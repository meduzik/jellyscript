#pragma once

// This file describes how to configure your engine build

// If config.user.h is available, it will be used to load 
// extra configuration values
#if __has_include("config.user.h")
	#include "config.user.h"
#endif

// Define JSC_DEBUG=1 if you want to use debug features
// By default, assumes 1 if NDEBUG is not defined
// #define JSC_DEBUG 1

// Define JSC_ASSERTS=1 if you want to enable asserts
// Equals to JSC_DEBUG by default
// #define JSC_ASSERTS 1
