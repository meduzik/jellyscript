#pragma once
#include <jsc/common/types.h>
#include <jsc/types/string_ref.h>
#include <jsc/memory/allocator.h>
#include <jsc/types/vector.h>

namespace jsc {

enum class ValueTag: u8 {
	Null,
	Bool,
	I64,
	U64,
	F32,
	F64,
	String,
	Array,
	Object
};

struct ObjectStream {
	ObjectStream(DynamicAllocator& alc) :
		data(&alc)
	{
	}

	ObjectStream& object_begin() {
		_object_begin();
		return *this;
	}

	ObjectStream& object_end() {
		_object_end();
		return *this;
	}
	
	ObjectStream& array_begin() {
		_array_begin();
		return *this;
	}

	ObjectStream& array_end() {
		_array_end();
		return *this;
	}

	ObjectStream& value(nullptr_t) {
		_value_null();
		return *this;
	}

	template<class T>
	std::enable_if_t<std::is_same_v<std::remove_all_extents_t<T>, bool>, ObjectStream&> value(T val) {
		_value_bool(val);
		return *this;
	}

	ObjectStream& value(u8 val) {
		_value_uint(val);
		return *this;
	}

	ObjectStream& value(u16 val) {
		_value_uint(val);
		return *this;
	}

	ObjectStream& value(u32 val) {
		_value_uint(val);
		return *this;
	}

	ObjectStream& value(u64 val) {
		_value_uint(val);
		return *this;
	}

	ObjectStream& value(i8 val) {
		_value_int(val);
		return *this;
	}

	ObjectStream& value(i16 val) {
		_value_int(val);
		return *this;
	}

	ObjectStream& value(i32 val) {
		_value_int(val);
		return *this;
	}

	ObjectStream& value(i64 val) {
		_value_int(val);
		return *this;
	}

	ObjectStream& value(float val) {
		_value_float(val);
		return *this;
	}

	ObjectStream& value(double val) {
		_value_double(val);
		return *this;
	}

	ObjectStream& value(StringRef val) {
		_value_string_begin();
		_feed_data(val);
		_value_string_end();
		return *this;
	}

	ObjectStream& string_begin() {
		_value_string_begin();
		return *this;
	}

	ObjectStream& string_data(StringRef val) {
		_feed_data(val);
		return *this;
	}

	ObjectStream& string_end() {
		_value_string_begin();
		return *this;
	}

	ObjectStream& key(StringRef val) {
		_key_begin();
		_feed_data(val);
		_key_end();
		return *this;
	}

	ObjectStream& key_begin() {
		_key_begin();
		return *this;
	}

	ObjectStream& key_data(StringRef val) {
		_feed_data(val);
		return *this;
	}

	ObjectStream& key_end() {
		_key_end();
		return *this;
	}

	template<class T>
	ObjectStream& field(StringRef k, T val) {
		return key(k).value(val);
	}
private:
	void _value_null();
	void _value_bool(bool val);
	void _value_uint(u64 val);
	void _value_int(i64 val);
	void _value_float(float val);
	void _value_double(double val);
	void _value_string_begin();
	void _value_string_end();
	void _key_begin();
	void _key_end();
	void _object_begin();
	void _object_end();
	void _array_begin();
	void _array_end();
	void _feed_data(StringRef data);

	dynamic_vector<u8> data;
};

}
