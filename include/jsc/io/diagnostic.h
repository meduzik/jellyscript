#pragma once
#include <jsc/types/source_info.h>
#include <jsc/common/types.h>
#include <jsc/io/object_stream.h>

namespace jsc {

template<class T>
auto to_object_stream(const T& val, ObjectStream& stream) -> decltype(stream.value(val), std::declval<void>()) {
	stream.value(val);
}

struct DiagnosticValueVT {
	template<class T>
	const static DiagnosticValueVT Instance;

	void (*to_object_stream_fn) (const void* val, ObjectStream& stream);
};

template<class T>
inline const DiagnosticValueVT DiagnosticValueVT::Instance{
	[](const void* p, ObjectStream& stream) {
		T* val = (T*)p;
		to_object_stream(*val, stream);
	}
};

struct DiagnosticValue {
public:
	template<class T>
	DiagnosticValue(const T& val):
		p(&val),
		vt(&DiagnosticValueVT::Instance<T>)
	{
	}

	void to_object_stream(ObjectStream& stream) {
		vt->to_object_stream_fn(p, stream);
	}
private:
	const void* p;
	const DiagnosticValueVT* vt;
};

struct DiagnosticArg {
	StringRef key;
	DiagnosticValue value;
};

enum class DiagnosticLevel: u32 {
	Info,
	Warning,
	Error
};

struct DiagnosticSink {
public:
	virtual void report(DiagnosticLevel level, SourceFile* file, SourceSpan span, StringRef code, Span<DiagnosticArg> args) = 0;
};

struct DiagnosticStream {
public:
	void report(DiagnosticLevel level, SourceFile* file, SourceSpan span, StringRef code, Span<DiagnosticArg> args) {
		sink->report(level, file, span, code, args);
	}

	void info(SourceFile* file, SourceSpan span, StringRef code, Span<DiagnosticArg> args) {
		report(DiagnosticLevel::Info, file, span, code, args);
	}

	void warning(SourceFile* file, SourceSpan span, StringRef code, Span<DiagnosticArg> args) {
		report(DiagnosticLevel::Warning, file, span, code, args);
	}

	void error(SourceFile* file, SourceSpan span, StringRef code, Span<DiagnosticArg> args) {
		report(DiagnosticLevel::Error, file, span, code, args);
	}
private:
	DiagnosticSink* sink;
};

}
