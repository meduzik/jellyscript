#pragma once

#include <jsc/common/types.h>
#include <jsc/macro/api.h>
#include <jsc/types/string_ref.h>

namespace jsc {

namespace format {

template<uword N>
struct FormatBuffer {
	static constexpr uword Size = N;

	operator StringRef() const {
		return StringRef(buffer, length);
	}

	u8 buffer[N];
	u16 length;
};

using IntegerBuffer = FormatBuffer<30>;

IntegerBuffer decimal(i64 val);
IntegerBuffer decimal(u64 val);

inline IntegerBuffer decimal(i8 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(i16 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(i32 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(u8 val) { return decimal((u64)val); }
inline IntegerBuffer decimal(u16 val) { return decimal((u64)val); }
inline IntegerBuffer decimal(u32 val) { return decimal((u64)val); }


using FloatBuffer = FormatBuffer<63>;
using DoubleBuffer = FormatBuffer<383>;

FloatBuffer scientific(float val);
DoubleBuffer scientific(double val);
FloatBuffer scientific(float val, uword precision);
DoubleBuffer scientific(double val, uword precision);

FloatBuffer fixed(float val);
DoubleBuffer fixed(double val);
FloatBuffer fixed(float val, uword precision);
DoubleBuffer fixed(double val, uword precision);

FloatBuffer shortest(float val, uword allowed_fixed_len = 6);
DoubleBuffer shortest(double val, uword allowed_fixed_len = 6);


}

}
