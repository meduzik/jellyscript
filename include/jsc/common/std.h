#pragma once

#include <cctype>
#include <cerrno>
#include <cfloat>
#include <climits>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <cfenv>
#include <cinttypes>
#include <cstdint>
#include <cwchar>
#include <cwctype>

// C++

#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <functional>
#include <iterator>
#include <limits>
#include <list>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <valarray>
#include <vector>
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <filesystem>
#include <charconv>

// RTTI is disabled
// #include <typeinfo>

// streams are not used
// #include <fstream>
// #include <iostream>
// #include <istream>
// #include <sstream>
// #include <ostream>
// #include <streambuf>
// #include <iosfwd>
// #include <iomanip>
// #include <ios>

// exceptions are disabled
// #include <stdexcept>
// #include <exception>
// regex cannot reliably work without exceptions
// #include <regex>

// #include <locale>
// #include <clocale>
