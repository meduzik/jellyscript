#pragma once
#include <jsc/macro/feature.h>
#include <jsc/memory/arena.h>
#include "string_ref.h"

namespace jsc {

struct Twine {
	Twine(StringRef sr) :
		tag(twine_tag::string_ref),
		value(sr) {
	}

	Twine(const Twine* lhs, const Twine* rhs) :
		tag(twine_tag::concat_tw_tw),
		value(lhs, rhs) {
	}

	Twine(const Twine* lhs, StringRef rhs) :
		tag(twine_tag::concat_tw_sr),
		value(lhs, rhs) {
	}

	Twine(StringRef lhs, const Twine* rhs) :
		tag(twine_tag::concat_sr_tw),
		value(lhs, rhs) {
	}

	Twine(StringRef lhs, StringRef rhs) :
		tag(twine_tag::concat_sr_sr),
		value(lhs, rhs) {
	}

	uword size() const {
		switch (tag) {
		case twine_tag::empty: return 0;
		case twine_tag::string_ref: return value.sr.size();
		case twine_tag::concat_tw_tw: return value.concat_tt.lhs->size() + value.concat_tt.rhs->size();
		case twine_tag::concat_sr_tw: return value.concat_srt.lhs.size() + value.concat_srt.rhs->size();
		case twine_tag::concat_tw_sr: return value.concat_tsr.lhs->size() + value.concat_tsr.rhs.size();
		case twine_tag::concat_sr_sr: return value.concat_srsr.lhs.size() + value.concat_srsr.rhs.size();
		default: jsc_unreachable;
		}
	}

	template<class Alc>
	StringRef allocate(Alc& alc) const {
		uword sz = size();
		u8* ptr = alc.allocate_checked(sz);
		write(ptr, this);
		return StringRef(ptr, sz);
	}

	template<class Alc>
	static void free(Alc& alc, StringRef sr) {
		alc.free(sr.data(), sr.size());
	}
private:
	static u8* write(u8* buf, const Twine* tw);
	static u8* write(u8* buf, StringRef sr);

	enum class twine_tag {
		empty,
		string_ref,
		concat_sr_sr,
		concat_sr_tw,
		concat_tw_sr,
		concat_tw_tw,
	} tag;
	union value_union {
		StringRef sr;
		struct {
			const Twine* lhs;
			const Twine* rhs;
		} concat_tt;
		struct {
			const Twine* lhs;
			StringRef rhs;
		} concat_tsr;
		struct {
			StringRef lhs;
			const Twine* rhs;
		} concat_srt;
		struct {
			StringRef lhs;
			StringRef rhs;
		} concat_srsr;

		constexpr value_union(StringRef sr) :
			sr(sr) {
		}
		constexpr value_union(const Twine* lhs, const Twine* rhs) :
			concat_tt{lhs, rhs} {
		}
		constexpr value_union(StringRef lhs, const Twine* rhs) :
			concat_srt{ lhs, rhs } {
		}
		constexpr value_union(const Twine* lhs, StringRef rhs) :
			concat_tsr{ lhs, rhs } {
		}
		constexpr value_union(StringRef lhs, StringRef rhs) :
			concat_srsr{ lhs, rhs } {
		}
	} value;
};

inline jsc_forceinline Twine operator+(const Twine& lhs, const Twine& rhs) {
	return Twine(&lhs, &rhs);
}

inline jsc_forceinline Twine operator+(const Twine& lhs, StringRef rhs) {
	return Twine(&lhs, rhs);
}

inline jsc_forceinline Twine operator+(StringRef lhs, const Twine& rhs) {
	return Twine(lhs, &rhs);
}

inline jsc_forceinline Twine operator+(StringRef lhs, StringRef rhs) {
	return Twine(lhs, rhs);
}

}

