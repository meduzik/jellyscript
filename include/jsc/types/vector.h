#pragma once

#include <jsc/common/std.h>
#include <jsc/memory/allocator.h>
#include <jsc/memory/arena.h>
#include <jsc/memory/pool.h>

namespace jsc {

template<class T>
using arena_vector = std::vector< T, AllocatorStdWrapper< Arena, T > >;

template<class T>
using pool_vector = std::vector< T, AllocatorStdWrapper< MemoryPool, T > >;

template<class T>
using dynamic_vector = std::vector< T, AllocatorStdWrapper< DynamicAllocator, T > >;

}

