#pragma once

#include "string_ref.h"

namespace jsc {

class PascalStringImpl {
public:
	uword size() const {
		return _size;
	}

	const u8* data() const {
		return _data;
	}

	operator StringRef() const{
		return {_data, _size};
	}

	template<class Alc>
	static PascalStringImpl* create(Alc& alc, StringRef ref) {
		u32 size = ref.size();
		PascalStringImpl* ps = new (alc.allocate(sizeof(u32) + size)) PascalStringImpl(ref);
		ps->_size = size;
		memcpy(ps->_data, ref.data(), size);
		return ps;
	}

	template<class Alc>
	void destroy(Alc& alc) {
		alc.free((u8*)this, _size + sizeof(u32));
	}
private:
	PascalStringImpl(StringRef ref):
		_size((u32)ref.size()) {
		memcpy(_data, ref.data(), ref.size());
	}

	u32 _size;
#if defined(JSC_COMPILER_MSVC)
#pragma warning(push)
#pragma warning(disable:4200)
#endif
	u8 _data[];
#if defined(JSC_COMPILER_MSVC)
#pragma warning(pop)
#endif
};

struct PascalString {
	PascalStringImpl* impl;

	uword size() const {
		return impl->size();
	}

	const u8* data() const {
		return impl->data();
	}

	operator StringRef() const {
		return impl->operator StringRef();
	}
	
	template<class Alc>
	static PascalString create(Alc& alc, StringRef ref) {
		return { PascalStringImpl::create(alc, ref) };
	}

	template<class Alc>
	void destroy(Alc& alc) {
		impl->destroy(alc);
	}
};

}
