#pragma once

#include <jsc/types/span.h>
#include <jsc/types/string_ref.h>

namespace jsc {

struct SourceSpan {
	u32 begin, end;

	SourceSpan(u32 begin, u32 end):
		begin(begin),
		end(end)
	{
	}
};

struct SourceLoc {
	u32 line;
	u32 col;

	static constexpr u32 InvalidValue = 0xffffu;
	static const SourceLoc InvalidLocation;
};

inline const struct SourceLoc SourceLoc::InvalidLocation = {
	SourceLoc::InvalidValue,
	SourceLoc::InvalidValue
};

class SourceMap {
public:
	SourceMap(Span<u32> lines) : lines(lines) {
	}

	SourceLoc get_loc(u32 pos) const;
	u32 get_pos(SourceLoc loc) const;

	SourceLoc get_end_loc() const;
private:
	Span<u32> lines;
};

struct SourceText {
	Span<u8> get_token(u32 idx) const;

	Span<u16> tokens;
	Span<u32> offsets;
	Span<u8> data;
};

struct SourceFile {
	StringRef filename;
	SourceMap map;
	SourceText text;

	SourceFile(StringRef filename, SourceMap map, SourceText text):
		filename(filename),
		map(map),
		text(text)
	{
	}
};

}
