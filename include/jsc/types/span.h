#pragma once

#include <jsc/common/std.h>
#include <jsc/common/types.h>
#include <jsc/macro/assert.h>
#include <jsc/macro/api.h>

namespace jsc{
	
template<class T>
class BaseSpan{
public:
	constexpr BaseSpan(std::nullptr_t):
		_begin(nullptr),
		_end(nullptr)
	{
	}

	constexpr BaseSpan(T* begin, T* end):
		_begin(begin),
		_end(end)
	{
	}

	constexpr BaseSpan(T* begin, uword size) :
		_begin(begin),
		_end(begin + size) {
	}

	constexpr BaseSpan(std::initializer_list<T> init) :
		_begin(init.begin()),
		_end(init.end()) {
	}

	template<size_t N>
	constexpr BaseSpan(T(&static_array)[N]):
		_begin(static_array),
		_end(static_array + N)
	{
	}

	constexpr T* begin() const{
		return _begin;
	}

	constexpr T* end() const{
		return _end;
	}

	constexpr T* data() const{
		return _begin;
	}

	constexpr uword size() const{
		return (_end - _begin);
	}

	constexpr T& operator[](uword index) const{
		jsc_assert(index < size());
		return _begin[index];
	}

	constexpr operator BaseSpan<const T>() const{
		return {_begin, _end};
	}

	template<class Alc>
	BaseSpan<T> copy(Alc& alc) const {
		using mut_T = std::remove_const_t<T>;
		mut_T* newp = (mut_T*)alc.allocate(sizeof(mut_T) * size());
		mut_T* q = newp;
		for (T* p = _begin; p != _end; p++, q++) {
			new(q)mut_T(*p);
		}
		return {newp, q};
	}
private:
	T* _begin;
	T* _end;
};

template<class T>
using Span = BaseSpan<const T>;

template<class T>
using MutSpan = BaseSpan<T>;

}

