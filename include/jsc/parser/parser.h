#pragma once

#include <jsc/memory/pool.h>
#include <jsc/io/diagnostic.h>

namespace jsc {

namespace ast {
struct File;
}

struct ParserJob {
	DiagnosticStream* diagnostic;
	StringRef filename;
	MemoryPool* mem;
	Span<u8> data;
};

struct Parser;

struct ParseResult {
	ast::File* ast;
	SourceFile* file;
};

Parser* parser_create();
ParseResult parser_run(Parser* parser, const ParserJob& job);
void parser_destroy(Parser* parser);

}
