#pragma once

#include "feature.h"
#include "api.h"
#include "panic.h"

// asserts that the current instruction in the program is unreachable
// panics with JSC_DEBUG, and is a noop (and undefined behavior) 
// otherwise
// compiler features
#if defined(JSC_COMPILER_MSVC)
	#if JSC_DEBUG
		#define jsc_unreachable jsc_panic("unreachable")
	#else
		#define jsc_unreachable __assume(false)
	#endif

	#define jsc_unlikely(x) (x)
	#define jsc_likely(x) (x)
#else
	#error Cannot infer compiler features
#endif
