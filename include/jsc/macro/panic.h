#pragma once

#include "api.h"

typedef void(*jsc_panic_callback)(void*, const char* file, int line, const char* function, const char* message);
typedef struct {
	void* ud;
	jsc_panic_callback func;
} jsc_panic_handler;
JSC_C jsc_panic_handler jsc_panic_handler_set(jsc_panic_handler new_handler);
JSC_C jsc_panic_handler jsc_panic_handler_get();
JSC_C jsc_noreturn jsc_noinline
void jsc_raise_panic(const char* file, int line, const char* function, const char* message);

// prints the message and the current location, and terminates the program
#define jsc_panic(message) (jsc_raise_panic(__FILE__, __LINE__, __FUNCTION__, (message)), (void)0)

