#pragma once

#include "feature.h"
#include "api.h"
#include "control_flow.h"

#if !defined(JSC_ASSERTS)
	#error JSC_ASSERTS must be defined
#endif

#define JSCZ_GET_MACRO(_1,_2,NAME,...) NAME
#define JSCZ_EXPAND(x) x

#if JSC_ASSERTS == 1
	#define JSCZ_ASSERT_2(condition, message) ( (condition) ? ((void)0) : jsc_panic("assertion '" message "' failed") )
	
	
#elif JSC_ASSERTS == 0
	#if defined(JSC_COMPILER_MSVC)
		#define JSCZ_ASSERT_2(condition, message) __assume(condition)
	#else
		#define JSCZ_ASSERT_2(condition, message) (void)0
	#endif
#else
	#error JSC_ASSERTS defined to an invalid value
#endif

#define JSCZ_ASSERT_1(condition) JSCZ_ASSERT_2(condition, #condition)

// checks the condition
// if the condition is false, then panics with the specified message, or
// the "assertion '" #cond "' failed" message
// if JSC_ASSERTS==0, jsc_assert is a noop
#define jsc_assert(...) JSCZ_EXPAND( JSCZ_GET_MACRO(__VA_ARGS__, JSCZ_ASSERT_2, JSCZ_ASSERT_1)(__VA_ARGS__) )




