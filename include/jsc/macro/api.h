#pragma once

#include "feature.h"


// jsc_noreturn
// Declares that a function never returns

#if defined(__cplusplus)
	#define JSC_C extern "C"
	#define jsc_noreturn [[noreturn]]
#else
	#define JSC_C
	#if defined(JSC_COMPILER_MSVC)
		#define jsc_noreturn __declspec(noreturn)
	#else
		#error Cannot infer jsc_noreturn macro for the compiler
	#endif
#endif

#if defined(JSC_COMPILER_MSVC)
	#define jsc_noinline __declspec(noinline)
	#define jsc_forceinline __forceinline
#else
	#define jsc_noinline __attribute__((noinline))
	#define jsc_forceinline __attribute__((always_inline))
#endif
