#pragma once

// This file performs feature detection on the environment

#include "../config.h"

#if defined(_MSC_VER)
	#define JSC_COMPILER_MSVC
#else
	#error Cannot detect the compiler!
#endif

#if defined(_WIN64)
	#define JSC_PLATFORM_WIN
	#define JSC_PLATFORM_WIN64
	#define JSC_PLATFORM_BIT 64
#elif defined(_WIN32)
	#define JSC_PLATFORM_WIN
	#define JSC_PLATFORM_WIN32
	#define JSC_PLATFORM_BIT 32
#else
	#error Cannot detect the platform!
#endif

#if !defined(JSC_PLATFORM_BIT)
	#error JSC_PLATFORM_BIT not defined
#elif JSC_PLATFORM_BIT == 64
	#define JSC_PLATFORM_64
#elif JSC_PLATFORM_BIT == 32
	#define JSC_PLATFORM_32
#else
	#error Invalid JSC_PLATFORM_BIT
#endif

#if !defined(JSC_DEBUG)
	#if defined(NDEBUG)
		#define JSC_DEBUG 0
	#else
		#define JSC_DEBUG 1
	#endif
#endif

#if !defined(JSC_ASSERTS)
	#if JSC_DEBUG
		#define JSC_ASSERTS 1
	#else
		#define JSC_ASSERTS 0
	#endif
#endif


