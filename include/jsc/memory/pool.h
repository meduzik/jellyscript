#pragma once

#include <jsc/common/types.h>
#include <jsc/types/span.h>
#include <jsc/memory/memory.h>
#include <jsc/memory/allocator.h>

namespace jsc {

/// A specialized arena allocator that doesn't allow savepoints.
/// Small 
struct MemoryPoolPolicy {
	size_t block_size = 64 * 1024;
	size_t large_size = 4 * 1024;
};

class MemoryPool: public Allocator<MemoryPool> {
	struct alignas(8) AllocationBlock {
		AllocationBlock* prev;
		uword size;
	};
public:
	MemoryPool(MemoryPoolPolicy policy = MemoryPoolPolicy()) :
		cur(nullptr),
		end(nullptr),
		block(nullptr),
		policy(policy) {
	}

	MemoryPool(MutSpan<u8> buffer, MemoryPoolPolicy policy = MemoryPoolPolicy()) :
		cur(buffer.data()),
		end(buffer.end()),
		block(nullptr),
		policy(policy)
	{
	}

	jsc_noinline
	~MemoryPool();
	
	u8* allocate(uword size) {
		uword round_size = (size + 7) & ~7;
		if ((uword)(end - cur) >= round_size) {
			u8* ptr = cur;
			cur += round_size;
			return ptr;
		} else {
			return allocate_slow(size);
		}
	}

	void free(u8*, uword) {
	}
private:
	jsc_noinline
	u8* allocate_slow(uword size);
	u8* allocate_block(uword total_size);

	u8* cur;
	u8* end;
	AllocationBlock* block;
	MemoryPoolPolicy policy;
};

}
