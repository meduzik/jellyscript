#pragma once
#include <jsc/common/types.h>
#include <jsc/memory/memory.h>

namespace jsc {


template<class Derived>
class Allocator {
public:
	u8* allocate_checked(uword size) {
		u8* p = ((Derived*)this)->Derived::allocate(size);
		if (size != 0 && !p) {
			jsc_panic("memory allocation failed");
		}
		return p;
	}

	u8* allocate(uword size) = delete;

	u8* reallocate(u8* ptr, uword old_size, uword new_size) {
		u8* new_ptr = ((Derived*)this)->allocate(new_size);
		if (!new_ptr) {
			return nullptr;
		}
		memcpy(new_ptr, ptr, std::min(old_size, new_size));
		((Derived*)this)->free(ptr, old_size);
		return new_ptr;
	}

	void free(u8* ptr, uword size) = delete;

	template<class T, class... Args>
	T* create(Args&&... args) {
		return new(((Derived*)this)->allocate_checked(sizeof(T))) T(std::forward<Args>(args)...);
	}

	template<class T>
	void destroy(T* object) {
		object->~T();
		((Derived*)this)->free((u8*)object, sizeof(T));
	}
};


template<class Alc, class T>
struct AllocatorStdWrapper : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef AllocatorStdWrapper<Alc, _Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)alc->allocate(sizeof(T) * n);
	}

	void deallocate(pointer p, size_type n) {
		alc->free((u8*)p, sizeof(T) * n);
	}

	AllocatorStdWrapper(Alc* alc) throw() : alc(alc) {}
	AllocatorStdWrapper(const AllocatorStdWrapper<Alc, T> &a) throw() = default;
	AllocatorStdWrapper(AllocatorStdWrapper<Alc, T> &&a) throw() = default;
	template <class U>
	AllocatorStdWrapper(const AllocatorStdWrapper<Alc, U>& a) throw() : alc(a.alc) {}
	template <class U>
	AllocatorStdWrapper(AllocatorStdWrapper<Alc, U>&& a) throw() : alc(a.alc) {}
	~AllocatorStdWrapper() throw() = default;
private:
	template<class U, class V>
	friend struct AllocatorStdWrapper;

	Alc* alc;
};


struct Mallocator: public Allocator<Mallocator> {
	u8* allocate(uword size) {
		return (u8*)memory_allocate(size);
	}

	void free(u8* ptr, uword size) {
		memory_free(ptr, size);
	}
};

struct DynamicAllocator {
	template<class Alc>
	static u8* wrap_allocate(void* alc_ptr, uword size) {
		Alc* alc = (Alc*)alc_ptr;
		return alc->allocate(size);
	}

	template<class Alc>
	static void wrap_free(void* alc_ptr, u8* ptr, uword size) {
		Alc* alc = (Alc*)alc_ptr;
		alc->free(ptr, size);
	}

	template<class Alc>
	DynamicAllocator(Alc& alc) :
		alc(&alc),
		fn_allocate(wrap_allocate<Alc>),
		fn_free(wrap_free<Alc>)
	{
	}

	u8* allocate(uword size) {
		return fn_allocate(alc, size);
	}

	void free(u8* ptr, uword size) {
		fn_free(alc, ptr, size);
	}
private:
	void* alc;
	u8* (*fn_allocate) (void* alc, uword size);
	void (*fn_free) (void* alc, u8* ptr, uword size);
};

inline Mallocator Mem;

}


