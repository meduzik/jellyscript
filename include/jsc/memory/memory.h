#pragma once

#include <jsc/common/types.h>
#include <jsc/macro/api.h>

namespace jsc {

inline jsc_forceinline u8* memory_allocate(uword size) {
	return (u8*)malloc(size);
}

inline jsc_forceinline u8* memory_reallocate(u8* ptr, uword old_size, uword new_size) {
	return (u8*)realloc(ptr, new_size);
}

inline jsc_forceinline void memory_free(u8* ptr, uword size) {
	free(ptr);
}



}
