#pragma once
#include <jsc/macro/api.h>
#include <jsc/macro/control_flow.h>
#include <jsc/common/types.h>
#include "allocator.h"

namespace jsc {

struct ArenaPoolState {
	u8* cur;
	u8* end;
};

struct alignas(std::max_align_t) ArenaLargeChunk {
	ArenaLargeChunk* next;
	uword size;
};

struct ArenaPool {
	ArenaPool(ArenaPoolState state);

	static ArenaPool* GetThreadLocal();
private:
	ArenaPoolState state;
	u32 level, maxlevel;
	u8* cache_chunk;

	friend struct Arena;
};

struct Arena: Allocator<Arena> {
	Arena();
	Arena(ArenaPool* pool);
	~Arena();

	Arena(const Arena&) = delete;
	Arena& operator=(const Arena&) = delete;

	u8* allocate(uword size) {
		auto pool = this->pool;
		uword taken_size = (size + 7) & ~7;
		auto cur = pool->state.cur;
		auto end = pool->state.end;			
		if (jsc_unlikely((uword)(end - cur) < taken_size)) {
			return allocate_slow_path(taken_size);
		}
		pool->level = std::min(pool->level, level);
		pool->state.cur = cur + taken_size;
		return cur;
	}

	void free(u8* ptr, uword size) {
	}
private:
	u8* allocate_slow_path(uword size);
	u8* allocate_large(uword size);

	ArenaPool* pool;
	ArenaPoolState reset;
	ArenaLargeChunk* large_chunk;
	u32 level, reset_level;
};

}

