#pragma once
#include "common.h"
#include "decl.h"
#include "function.h"
#include "var.h"

namespace jsc::ast {

enum class AggregateKind {
	Interface,
	Struct,
	Class,
	RefStruct
};

struct AggregateMember;

struct DeclarationAggregate : public DeclarationBase<Declaration::Tag::Aggregate> {
	AggregateKind kind;
	Span<Expr*> bases;
	Span<AggregateMember*> members;

	DeclarationAggregate(
		SymbolInfo si,
		AggregateKind kind,
		Span<Expr*> bases,
		Span<AggregateMember*> members
	) :
		Base(si),
		kind(kind),
		bases(bases),
		members(members)
	{
	}
};


struct AggregateMember : SymbolInfo {
	enum class Tag {
		Method,
		Field
	} tag;

	AggregateMember(
		Tag tag,
		SymbolInfo si
	) :
		SymbolInfo(si),
		tag(tag) {
	}
};

SYNTHESIZE_MATCH_BASE(AggregateMemberBase, AggregateMember);

struct AggregateMethod : public AggregateMemberBase<AggregateMember::Tag::Method>, Function {
	AggregateMethod(
		SymbolInfo si,
		Function fn
	) :
		Base(si),
		Function(fn) {
	}
};

struct AggregateField : public AggregateMemberBase<AggregateMember::Tag::Field>, VariableDecl {
	AggregateField(
		SymbolInfo si,
		VariableDecl var
	) :
		Base(si),
		VariableDecl(var) {
	}
};

}