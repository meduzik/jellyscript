#pragma once
#include "var.h"

namespace jsc::ast {

struct Expr {
	enum class Tag {
		ID,
		IDRef,
		Call,
		Subscript,
		PrefixOp,
		PostfixOp,
		BinOp,
		Conditional,
		Lit,
		Var
	} tag;
	TokenRef begin;
	TokenRef end;
	Expr(
		Tag tag,
		TokenRef begin,
		TokenRef end
	) :
		tag(tag)
	{
	}
};

SYNTHESIZE_MATCH_BASE(ExprBase, Expr);

enum class PrefixOp {
	Plus,
	Minus,
	BitInvert,
	LogInvert,
	Increment,
	Decrement
};

enum class PostfixOp {
	Increment,
	Decrement
};

enum class BinOp {
	Mul,
	Div,
	Mod,
	Add,
	Sub,
	Shl,
	AShr,
	LShr,
	LT,
	GT,
	LE,
	GE,
	EQ,
	NEQ,
	BitAnd,
	BitXor,
	BitOr,
	LogAnd,
	LogOr,
	Assign,
	AssignAdd,
	AssignSub,
	AssignMul,
	AssignDiv,
	AssignMod,
	AssignShl,
	AssignAShr,
	AssignLShr,
	AssignBitAnd,
	AssignBitXor,
	AssignBitOr
};

struct ExprID: ExprBase<Expr::Tag::ID> {
	ExprID(TokenRef name) : ExprBase(name, name) {
	}
};

struct ExprLit : ExprBase<Expr::Tag::Lit> {
	ExprLit(TokenRef tok) : ExprBase(tok, tok) {
	}
};

struct ExprIDRef : ExprBase<Expr::Tag::IDRef> {
	Expr* expr;

	ExprIDRef(Expr* expr, TokenRef name) : ExprBase(expr->begin, name) {
	}
};

struct ExprCall : ExprBase<Expr::Tag::Call> {
	Expr* head;
	Span<Expr*> args;

	ExprCall(TokenRef end, Expr* head, Span<Expr*> args) :
		ExprBase(head->begin, end),
		head(head),
		args(args) 
	{
	}
};

struct ExprSubscript : ExprBase<Expr::Tag::Subscript> {
	Expr* head;
	Span<Expr*> args;

	ExprSubscript(TokenRef end, Expr* head, Span<Expr*> args):
		ExprBase(head->begin, end),
		head(head),
		args(args)
	{
	}
};

struct ExprPrefixOp : ExprBase<Expr::Tag::PrefixOp> {
	PrefixOp op;
	Expr* expr;

	ExprPrefixOp(TokenRef begin, PrefixOp op, Expr* expr) :
		ExprBase(begin, expr->end),
		op(op),
		expr(expr) {
	}
};

struct ExprPostfixOp : ExprBase<Expr::Tag::PostfixOp> {
	PostfixOp op;
	Expr* expr;

	ExprPostfixOp(TokenRef end, PostfixOp op, Expr* expr) :
		ExprBase(expr->begin, end),
		op(op),
		expr(expr) {
	}
};

struct ExprBinOp : ExprBase<Expr::Tag::BinOp> {
	Expr* lhs;
	Expr* rhs;
	BinOp op;

	ExprBinOp(Expr* lhs, Expr* rhs, BinOp op) :
		ExprBase(lhs->begin, rhs->end),
		lhs(lhs),
		rhs(rhs),
		op(op) {
	}
};

struct ExprConditional : ExprBase<Expr::Tag::Conditional> {
	Expr* cond;
	Expr* lhs;
	Expr* rhs;

	ExprConditional(Expr* cond, Expr* lhs, Expr* rhs) :
		ExprBase(cond->begin, rhs->end),
		lhs(lhs),
		rhs(rhs),
		cond(cond) {
	}
};

struct ExprVar : ExprBase<Expr::Tag::Var> {
	VariableDecl decl;

	ExprVar(TokenSpan span, VariableDecl decl) :
		ExprBase(span.begin, span.end),
		decl(decl)
	{
	}
};

}
