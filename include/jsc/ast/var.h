#pragma once
#include "common.h"
#include "decl.h"

namespace jsc::ast {

struct VariableGroup {
	Span<TokenRef> names;
	Expr* type;
	Expr* initializer;

	VariableGroup(Span<TokenRef> names, Expr* type, Expr* initializer):
		names(names),
		type(type),
		initializer(initializer)
	{
	}
};

enum class VariableKind {
	Const,
	Var
};

struct VariableDecl {
	VariableKind kind;
	Span<VariableGroup*> groups;

	VariableDecl(VariableKind kind, Span<VariableGroup*> groups):
		kind(kind),
		groups(groups)
	{
	}
};

struct DeclarationVariable : public DeclarationBase<Declaration::Tag::Variable>, VariableDecl {
	DeclarationVariable(
		SymbolInfo si,
		VariableDecl var
	) :
		Base(si),
		VariableDecl(var) {
	}
};

}
