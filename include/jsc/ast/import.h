#pragma once
#include "common.h"

namespace jsc::ast {


struct ImportAsClause {
	TokenRef name;

	ImportAsClause(TokenRef name) : name(name) {
	}
};

struct ImportName {
	TokenRef orig_name;
	ImportAsClause* as_clause;

	ImportName(TokenRef orig_name, ImportAsClause* as_clause) :
		orig_name(orig_name),
		as_clause(as_clause)
	{
	}
};

struct ImportListClause {
	Span<ImportName*> names;

	ImportListClause(Span<ImportName*> names):
		names(names)
	{
	}
};

struct ImportDecl {
	TokenRef path;
	ImportAsClause* as_clause;
	ImportListClause* list_clause;

	ImportDecl(TokenRef path, ImportAsClause* as_clause, ImportListClause* list_clause) :
		path(path),
		as_clause(as_clause),
		list_clause(list_clause) 
	{
	}
};


}
