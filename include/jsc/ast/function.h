#pragma once
#include "decl.h"

namespace jsc::ast {

struct FunctionSignature {
	TokenSpan span;
	Span<ParamGroup*> params;
	Expr* ret_ty;

	FunctionSignature(TokenSpan span, Span<ParamGroup*> params, Expr* ret_ty) :
		span(span),
		params(params),
		ret_ty(ret_ty)
	{
	}
};

struct FunctionBody {
	enum class Tag {
		Block,
		Expr
	} tag;
	TokenSpan span;

	FunctionBody(
		Tag tag,
		TokenSpan span
	) :
		tag(tag),
		span(span) {
	}
};
SYNTHESIZE_MATCH_BASE(FunctionBodyBase, FunctionBody);


struct FunctionBodyBlock : public FunctionBodyBase<FunctionBody::Tag::Block> {
	Block* block;

	FunctionBodyBlock(
		TokenSpan span,
		Block* block
	) :
		Base(span),
		block(block) {
	}
};

struct FunctionBodyExpr : public FunctionBodyBase<FunctionBody::Tag::Expr> {
	Expr* expr;

	FunctionBodyExpr(
		TokenSpan span,
		Expr* expr
	) :
		Base(span),
		expr(expr) {
	}
};

struct Function {
	FunctionSignature* signature;
	FunctionBody* body;

	Function(
		FunctionSignature* signature,
		FunctionBody* body
	) :
		signature(signature),
		body(body) {
	}
};

struct DeclarationFunction : public DeclarationBase<Declaration::Tag::Function>, Function {
	DeclarationFunction(
		SymbolInfo si,
		Function fn
	) :
		Base(si),
		Function(fn) {
	}
};

}
