#pragma once

#include <jsc/common/types.h>
#include <jsc/types/span.h>
#include <jsc/types/source_info.h>
#include <jsc/macro/match.h>
#include <jsc/memory/pool.h>

namespace jsc::ast {

struct Expr;
struct Stmt;

using TokenRef = u32;

struct TokenSpan {
	u32 begin, end;

	TokenSpan(u32 begin, u32 end) :
		begin(begin),
		end(end)
	{
	}
};

inline constexpr TokenRef NullTokenRef = ~0u;

struct QualID {
	Span<TokenRef> ids;

	QualID(Span<TokenRef> ids) : ids(ids) {
	}
};

struct CallArg {
	TokenRef m_name;
	Expr* val;
};

struct CallArgs {
	Span<CallArg> args;

	CallArgs(Span<CallArg> args): args(args) {
	}
};

struct Annotation {
	TokenSpan span;
	QualID* name;
	CallArgs* args;

	Annotation(TokenSpan span, QualID* name, CallArgs* args):
		span(span),
		name(name),
		args(args) {
	}
};

struct Annotations {
	Span<Annotation*> list;

	Annotations(Span<Annotation*> list) : list(list) {
	}
};

enum class AccessModifier : u8 {
	Public,
	Private,
	Protected,
	Internal,
	Default
};

enum class Modifiers: u8 {
	Abstract = (1 << 0),
	Final = (1 << 1),
	Virtual = (1 << 2),
	Override = (1 << 3),
	Static = (1 << 4)
};

struct SymbolInfo {
	Annotations* annotations;
	TokenSpan span;
	AccessModifier access;
	Modifiers modifiers;
	TokenRef name;

	SymbolInfo(
		Annotations* annotations,
		TokenSpan span,
		AccessModifier access,
		Modifiers modifiers,
		TokenRef name
	) :
		annotations(annotations),
		span(span),
		access(access),
		modifiers(modifiers),
		name(name) {
	}
};


struct ParamGroup {
	Span<TokenRef> names;
	Expr* type;
	Expr* initializer;

	ParamGroup(Span<TokenRef> names, Expr* type, Expr* initializer) :
		names(names),
		type(type),
		initializer(initializer)
	{
	}
};

struct Block {
	TokenSpan span;
	Span<Stmt*> stmts;

	Block(TokenSpan span, Span<Stmt*> stmts):
		span(span),
		stmts(stmts)
	{
	}
};

}
