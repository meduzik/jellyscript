#pragma once
#include "expr.h"

namespace jsc::ast {

struct Stmt {
	enum class Tag {
		Expr,
		Return,
		If,
		While,
		Block,
		Var,
		For,
		Break,
		Continue,
		Label,
		Goto
	} tag;
	TokenSpan span;
	Stmt(
		Tag tag,
		TokenSpan span
	) :
		tag(tag),
		span(span)
	{
	}
};

SYNTHESIZE_MATCH_BASE(StmtBase, Stmt);

struct StmtExpr : StmtBase<Stmt::Tag::Expr> {
	Expr* expr;

	StmtExpr(TokenSpan span, Expr* expr):
		StmtBase(span),
		expr(expr)
	{
	}
};

struct StmtReturn : StmtBase<Stmt::Tag::Return> {
	Expr* expr;

	StmtReturn(TokenSpan span, Expr* expr):
		StmtBase(span),
		expr(expr)
	{
	}
};

struct IfBranch {
	Span<Expr*> cond;
	Block* block;

	IfBranch(Span<Expr*> cond, Block* block) :
		cond(cond),
		block(block)
	{
	}
};

struct StmtIf : StmtBase<Stmt::Tag::If> {
	Span<Expr*> exprs;
	Block* then_block;
	Span<IfBranch*> else_branches;

	StmtIf(TokenSpan span, Span<Expr*> exprs, Block* then_block, Span<IfBranch*> else_branches) :
		StmtBase(span),
		exprs(exprs),
		then_block(then_block),
		else_branches(else_branches)
	{
	}
};

struct StmtWhile : StmtBase<Stmt::Tag::While> {
	Span<Expr*> exprs;
	Block* block;

	StmtWhile(TokenSpan span, Span<Expr*> exprs, Block* block) :
		StmtBase(span),
		exprs(exprs),
		block(block)
	{
	}
};

struct StmtBlock : StmtBase<Stmt::Tag::Block> {
	Block* block;

	StmtBlock(Block* block) :
		StmtBase(block->span),
		block(block) {
	}
};

struct StmtVar : StmtBase<Stmt::Tag::Var> {
	VariableDecl decl;

	StmtVar(TokenSpan span, VariableDecl decl) :
		StmtBase(span),
		decl(decl) {
	}
};

struct StmtBreak : StmtBase<Stmt::Tag::Break> {
	TokenRef label;

	StmtBreak(TokenSpan span, TokenRef label) :
		StmtBase(span),
		label(label) {
	}
};

struct StmtGoto : StmtBase<Stmt::Tag::Goto> {
	TokenRef label;

	StmtGoto(TokenSpan span, TokenRef label) :
		StmtBase(span),
		label(label) {
	}
};

struct StmtContinue : StmtBase<Stmt::Tag::Continue> {
	TokenRef label;

	StmtContinue(TokenSpan span, TokenRef label) :
		StmtBase(span),
		label(label) {
	}
};

struct StmtLabel : StmtBase<Stmt::Tag::Label> {
	TokenRef label;

	StmtLabel(TokenSpan span, TokenRef label) :
		StmtBase(span),
		label(label) {
	}
};


struct StmtFor : StmtBase<Stmt::Tag::For> {
	Span<Expr*> init;
	Span<Expr*> cond;
	Span<Expr*> incr;
	Block* block;

	StmtFor(TokenSpan span, Span<Expr*> init, Span<Expr*> cond, Span<Expr*> incr, Block* block) :
		StmtBase(span),
		init(init),
		cond(cond),
		incr(incr),
		block(block) {
	}
};

}
