#pragma once
#include "common.h"

namespace jsc::ast {

struct ImportDecl;
struct Declaration;

struct File {
	Span<ImportDecl*> imports;
	Span<Declaration*> declarations;

	File(
		Span<ImportDecl*> imports,
		Span<Declaration*> declarations
	) :
		imports(imports),
		declarations(declarations)
	{
	}
};


}
