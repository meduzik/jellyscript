#pragma once
#include "common.h"

namespace jsc::ast {


struct Declaration : SymbolInfo {
	enum class Tag {
		Aggregate,
		Function,
		Variable
	} tag;

	Declaration(
		Tag tag,
		SymbolInfo si
	) :
		SymbolInfo(si),
		tag(tag) {
	}
};

SYNTHESIZE_MATCH_BASE(DeclarationBase, Declaration);


}